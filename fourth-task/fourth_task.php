<html>
    <head>
        <title>DIBZ - Fourth Task</title>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    
		<link rel="icon" href="../res/images/dibz-logo.png">
		<link rel="stylesheet" href="/res/css/reset.css" >
	    <link href="../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="fourth_task.css" >
	    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="fourth_task.js" type="text/javascript"></script>
    </head>
    <body>

    	<div class="container">
        	<div class="row">
		  	<?php
		    	for ($x = 1; $x < 5; $x++) :
		   	?>
	    		<div class="col-xs-3 content">
		        	<img class="img-cat img-responsive" src="../res/images/cat1.png">
		            <h3 class="counter">0</h3>
		            <button class="btn btn-block btn-success btn-click" data-click="add"><i class="fa fa-paw"></i> click me</button>
		            <button class="btn btn-block btn-danger btn-reset" data-click="reset"><i class="fa fa-refresh"></i> reset</butto>
	        	</div>
		    <?php
		    	endfor;
		  	?>
            </div>
    	</div>

    </body>
</html>