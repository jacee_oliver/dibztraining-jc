$(function() {

    $('.btn-click, .btn-reset').on('click', function() {
    	var _this = $(this),
        	_parent = _this.closest('.content'),
            _counter = _parent.find('.counter'),
            _img = _parent.find('.img-cat'),
            _val = _counter.text();

        _this.hasClass('btn-click') ? _counter.html(++_val) : _counter.html(0);

        if (_this.hasClass('btn-click')) {
    		_img.attr("src", _val % 2 ? "../res/images/cat2.png" : "../res/images/cat3.png");
        	return true;
        }

    	_img.attr("src", "../res/images/cat1.png");

    });

});
